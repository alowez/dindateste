//
//  ContasLista.m
//  teste
//
//  Created by Aloisio Mello on 4/8/16.
//  Copyright © 2016 GAS Tecnologia. All rights reserved.
//

#import "ContasLista.h"

@implementation ContasLista

-(id)initWithFileName : (NSString*)file{
    NSString* fileContents = [NSString stringWithContentsOfFile:file
                                                       encoding:NSUTF8StringEncoding
                                                          error:nil];
    NSArray *arrFileName = [fileContents componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]];
    
    self.arrContas = [[NSMutableArray alloc] initWithCapacity:0];
    
    for(NSString * line in arrFileName){
        Contas * contas = [[Contas alloc] initWithString:line];
        [self.arrContas addObject:contas];
    }
    
    return self;
}

-(void)executeTransactionWithTransacoesObject:(Transacoes*)transacoes{
    BOOL accountExists = NO;
    BOOL isDeposit = YES;
    
    if (transacoes.saldo < 0)
        isDeposit = NO;
    
    for(Contas * contas in self.arrContas){
        if(contas.idConta == transacoes.idConta){
            contas.saldo += transacoes.saldo;
            if(contas.saldo < 0 && !isDeposit){
                contas.saldo -= 5;
            }
            accountExists = YES;
        }
    }
    
    if(!accountExists)
        NSLog(@"Não foi possível encontrar a conta %d",transacoes.idConta);
}
@end
