//
//  Contas.m
//  teste
//
//  Created by Aloisio Mello on 4/8/16.
//  Copyright © 2016 GAS Tecnologia. All rights reserved.
//

#import "Contas.h"

@implementation Contas

-(id)initWithString : (NSString*)string{
    NSArray * arrObjects = [string componentsSeparatedByString:@","];
    if([arrObjects count] > 1){
        self.idConta = (int)[[arrObjects objectAtIndex:0] integerValue];
        self.saldo = (int)[[arrObjects objectAtIndex:1] integerValue];
    }
    
    return self;
}
@end
