//
//  TransacoesLista.m
//  teste
//
//  Created by Aloisio Mello on 4/8/16.
//  Copyright © 2016 GAS Tecnologia. All rights reserved.
//

#import "TransacoesLista.h"

@implementation TransacoesLista

-(id)initWithFileName : (NSString*)file{
    NSString* fileContents = [NSString stringWithContentsOfFile:file
                                                       encoding:NSUTF8StringEncoding
                                                          error:nil];
    NSArray *arrFileName = [fileContents componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]];
    
    self.arrTransaction = [[NSMutableArray alloc] initWithCapacity:0];
    
    for(NSString * line in arrFileName){
        Transacoes * transacoes = [[Transacoes alloc] initWithString:line];
        [self.arrTransaction addObject:transacoes];
    }
    
    return self;
}

@end
