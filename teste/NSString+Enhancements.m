//
//  NSString+Enhancements.m
//  teste
//
//  Created by Aloisio Mello on 4/12/16.
//  Copyright © 2016 GAS Tecnologia. All rights reserved.
//

#import "NSString+Enhancements.h"

@implementation NSString (Enhancements)

+(BOOL)isNullOrEmpty:(NSString *)inString
{
    BOOL retVal = YES;
    
    if( inString != nil )
    {
        if( [inString isKindOfClass:[NSString class]] )
        {
            retVal = inString.length == 0;
        }
        else
        {
            NSLog(@"isNullOrEmpty, value not a string");
        }
    }
    return retVal;
}

@end
